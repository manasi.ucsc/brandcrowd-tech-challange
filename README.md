Test scenarios:

1. Default search
2. Successfully search by Business name
3. Successfully search by Keywords
4. Expand more search fields
5. Successfully search by Logo style
6. Successfully search by one Font style
7. Successfully search by more than one Font style
8. Successfully search by Color
9. Successfully search by more than one Color
10. Successfully search by multiple search enties
11. Reset search fields  
