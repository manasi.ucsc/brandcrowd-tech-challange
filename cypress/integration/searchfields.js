describe('Search functionality', () => {

    const keyword_one = 'Apple'
    const keyword_two = 'Orange'
    const buttontext = 'create logos'
    const business_one = 'ABC Business'
    const business_two = 'DEF Business'
    const logostyle_one = 'Icon & Text'
    const logostyle_two = 'Text-only'
    const fontstyle_one = 'bold'
    const fontstyle_two = 'vintage'
    const color_one = 'grayscale'
    const color_two = 'blue'

    beforeEach(() => {
        cy.visit('/');
    });

    it('user should be able to search by keyword', () => {
        cy.get('input[id="SearchText"]').type(keyword_one);
        cy.get('button').contains(buttontext).click();
        cy.get('a[href*="/"]').should('contain.text', keyword_one);

        cy.get('input[id="SearchText"]').clear().type(keyword_two);
        cy.get('button').contains(buttontext).click();
        cy.get('a[href*="/"]').should('contain.text', keyword_two);
    });

    it('user should be able to search by business name', () => {
        cy.get('input[id="gtm-SearchText"]').type(business_one);
        cy.get('button').contains(buttontext).click();
        cy.get('.text-center').should('contain.text', business_one);

        cy.get('input[id="gtm-SearchText"]').clear().type(business_two);
        cy.get('button').contains(buttontext).click();
        cy.get('.text-center').should('contain.text', business_two);
    });
    
     it('user should be able to search by logo style', () => {
        cy.get('span[id="filter-bar__more-button"]').click();
        cy.get('select[id="LogoStyle"]').select(logostyle_one).should('have.value', '1');
        cy.get('button').contains(buttontext).click();
        cy.get('a[href*="/"]').should('contain.text', 'Professional Car Insurance Security');

        cy.get('select[id="LogoStyle"]').select(logostyle_two).should('have.value', '2');
        cy.get('button').contains(buttontext).click();
        cy.get('a[href*="/"]').should('contain.text', 'Simple Green Apple Lettermark');
    });

    it('user should be able to search by font style', () => {
        cy.get('span[id="filter-bar__more-button"]').click();
        cy.get('[type="checkbox"]').check(fontstyle_one,{force: true})
        cy.get('[type="checkbox"]').check(fontstyle_two,{force: true})
        cy.get('button').contains(buttontext).click();
        cy.get('a[href*="/"]').should('contain.text', 'Art School Pencil');
    });

    it('user should be able to search by font color', () => {
        cy.get('span[id="filter-bar__more-button"]').click();
        cy.get('[type="checkbox"]').check(color_two,{force: true})
        cy.get('[type="checkbox"]').check(color_two,{force: true})
        cy.get('button').contains(buttontext).click();
        cy.get('a[href*="/"]').should('contain.text', 'Youtube Technology Vlog');
    });

    it('user should be able to search by combination of serach entries', () => {
        cy.get('input[id="SearchText"]').type(keyword_one);
        cy.get('span[id="filter-bar__more-button"]').click();
        cy.get('select[id="LogoStyle"]').select(logostyle_one).should('have.value', '1');
        cy.get('[type="checkbox"]').check(fontstyle_one,{force: true})
        cy.get('[type="checkbox"]').check(color_one,{force: true})
        cy.get('button').contains(buttontext).click();
        cy.get('a[href*="/"]').should('contain.text', 'Origami Apple');
    });
});
